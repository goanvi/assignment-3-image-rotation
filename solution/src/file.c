#include "../include/file.h"

enum file_open_status file_open(FILE** const file, char const * const file_name, char const * const mode){
    *file = fopen(file_name, mode);
    if ((*file)==NULL)  return FL_OPEN_ERR;
    return FL_OPEN_OK;
}
enum file_close_status file_close(FILE* const file){
    if (!fclose(file))  return FL_CLOSE_OK;
    return FL_CLOSE_ERR;

}
