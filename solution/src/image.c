#include "../include/image.h"
#include <stdlib.h>
struct image image_create(uint64_t const width, uint64_t const height){
    struct image image = {
        .width = width,
        .height = height,
        .data = malloc(sizeof(struct pixel) * width * height)
    };
    return image;
    
}
void image_destroy(struct image* const image){
    free(image->data);
}

struct pixel get_pixel(struct image const* img ,uint64_t const offset, uint64_t const row){
    return img->data[img->width*row + offset];
}

void set_pixel(struct image* img ,uint64_t const offset, uint64_t const row ,struct pixel pixel){
    img->data[img->width*row + offset] = pixel;
}

