#include "../include/bmp.h"
#include "../include/file.h"
#include "../include/image.h"
#include "../include/rotate.h"

int main( int argc, char** argv ) {
    if (argc != 3) {
    fprintf(stderr, "Incorrect number of parametrs: expected: 2, income: %d\n", argc);
    return 1;
    }

    FILE* in = NULL;
    FILE* out = NULL;
    
    if (file_open(&in, argv[1], "rb")) {
        fprintf(stderr, "Can not open input file\n");
        return 1;
    }
    
    if (file_open(&out, argv[2], "wb")) {
        fprintf(stderr, "Can not open output file\n");
        file_close(in);
        return 1;
    }
    
    struct image img = {0};
    if (from_bmp(in, &img)!=READ_OK){
        fprintf(stderr, "Can not read input file\n");
        file_close(in);
        file_close(out);
        return 1;
    }
	
	struct image rotated = rotate(&img);
	if (to_bmp(out, &rotated) != WRITE_OK) {
		fprintf(stderr, "Can not write to output file\n");
		image_destroy(&rotated);
        image_destroy(&img);
		file_close(in);
		file_close(out);
		return 1;
	}
		
	if (file_close(in) || file_close(out)) {
		fprintf(stderr, "Can not close open files\n");
		image_destroy(&img);
		image_destroy(&rotated);
		return 1;
	}
	image_destroy(&img);
	image_destroy(&rotated);	
    return 0;
}
