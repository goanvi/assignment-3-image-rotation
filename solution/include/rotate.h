#ifndef LAB_3_ROTATE_H
#define LAB_3_ROTATE_H
#include "./image.h"
#include <stdio.h>


struct image rotate( struct image const* const src );

#endif
