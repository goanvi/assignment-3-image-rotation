#ifndef LAB_3_IMAGE_H
#define LAB_3_IMAGE_H
#include  <stdint.h>

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image image_create(uint64_t const width, uint64_t const height);

void image_destroy(struct image* const image);

struct pixel get_pixel(struct image const* img ,uint64_t const offset, uint64_t const row);

void set_pixel(struct image* img ,uint64_t const offset, uint64_t const row, struct pixel pixel);

#endif
